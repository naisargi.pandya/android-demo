package com.android_demo.helper.util

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.android_demo.AppConstants.Prefs.AUTH_TOKEN
import com.android_demo.AppConstants.Prefs.FCM_TOKEN
import com.android_demo.AppConstants.Prefs.ROOM_KEY
import com.android_demo.AppConstants.Prefs.USER_INFO
import com.android_demo.Strings
import com.android_demo.data.model.response.LoginResModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject

class PrefUtil
@Inject
constructor(@ApplicationContext context: Context) {

    private val prefs: SharedPreferences = EncryptedSharedPreferences.create(
        context,
        context.getString(Strings.app_name),
        MasterKey.Builder(context).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build(),
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )

    private val prefEditor: SharedPreferences.Editor

    init {
        prefEditor = prefs.edit()
    }

    var authToken: String?
        get() = prefs.getString(AUTH_TOKEN, "")
        set(authToken) {
            prefEditor.putString(AUTH_TOKEN, authToken)
            prefEditor.apply()
        }

    var roomKey: String
        get() = prefs.getString(ROOM_KEY, "").orEmpty()
        set(key) {
            prefEditor.putString(ROOM_KEY, key)
            prefEditor.apply()
        }

    var fcmToken: String?
        get() = prefs.getString(FCM_TOKEN, "")
        set(fcmToken) {
            prefEditor.putString(FCM_TOKEN, fcmToken)
            prefEditor.apply()
        }

    var userInfo: LoginResModel?
        get() = prefs.getString(USER_INFO, "")?.convertToModel()
        set(data) {
            prefEditor.putString(USER_INFO, Json.encodeToString(data))
            prefEditor.apply()
        }

    fun hasKey(key: String) = prefs.contains(key)

    fun clearPrefs() {
        prefs.all.forEach {
            prefEditor.remove(it.key)
        }
        prefEditor.apply()
    }
}
