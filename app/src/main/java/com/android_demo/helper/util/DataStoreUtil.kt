package com.android_demo.helper.util

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.MutablePreferences
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import com.android_demo.AppConstants.DataStore.AUTH_TOKEN
import com.android_demo.AppConstants.DataStore.FCM_TOKEN
import com.android_demo.AppConstants.DataStore.ROOM_KEY
import com.android_demo.AppConstants.DataStore.USER_INFO
import com.android_demo.data.model.response.LoginResModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject

class DataStoreUtil
@Inject
constructor(
    private val dataStore: DataStore<Preferences>,
    private val security: SecurityUtil
) {
    private val securityKeyAlias = "data-store"
    private val bytesToStringSeparator = "|"

    /**
     * for synchronicity [not recommended for heavy operations]
     */
    /*var authToken: String
        get() = runBlocking {
            dataStore.data.first()[AUTH_TOKEN].orEmpty()
        }
        set(authToken) {
            runBlocking {
                dataStore.edit {
                    it[AUTH_TOKEN] = authToken
                }
            }
        }*/

    //region auth token
    fun getAuthToken() = dataStore.data
        .secureMap<String> { preferences ->
            preferences[AUTH_TOKEN].orEmpty()
        }

    suspend fun setAuthToken(value: String) {
        dataStore.secureEdit(value) { prefs, encryptedValue ->
            prefs[AUTH_TOKEN] = encryptedValue
        }
    }
    //endregion

    //region room key
    fun getRoomToken() = dataStore.data
        .secureMap<String> { preferences ->
            preferences[ROOM_KEY].orEmpty()
        }

    suspend fun setRoomToken(value: String) {
        dataStore.secureEdit(value) { prefs, encryptedValue ->
            prefs[ROOM_KEY] = encryptedValue
        }
    }
    //endregion

    //region fcm token
    fun getFcmToken() = dataStore.data
        .secureMap<String> { preferences ->
            preferences[FCM_TOKEN].orEmpty()
        }

    suspend fun setFcmToken(value: String) {
        dataStore.secureEdit(value) { prefs, encryptedValue ->
            prefs[FCM_TOKEN] = encryptedValue
        }
    }
    //endregion

    //region user info
    fun getUserInfo() = dataStore.data
        .secureMap<LoginResModel> { prefs ->
            prefs[USER_INFO].orEmpty()
        }

    suspend fun setUserInfo(value: LoginResModel) {
        dataStore.secureEdit(value) { prefs, encryptedValue ->
            prefs[USER_INFO] = encryptedValue
        }
    }
    //endregion

    suspend fun hasKey(key: Preferences.Key<*>) = dataStore.edit { it.contains(key) }

    suspend fun clearDataStore() {
        dataStore.edit {
            it.clear()
        }
    }

    private inline fun <reified T> Flow<Preferences>.secureMap(crossinline fetchValue: (value: Preferences) -> String): Flow<T> {
        return map {
            val decryptedValue = security.decryptData(
                securityKeyAlias,
                fetchValue(it).split(bytesToStringSeparator).map { it.toByte() }.toByteArray()
            )
            Json { encodeDefaults = true }.decodeFromString<T>(decryptedValue)
        }
    }

    private suspend inline fun <reified T> DataStore<Preferences>.secureEdit(
        value: T,
        crossinline editStore: (MutablePreferences, String) -> Unit
    ) {
        edit {
            val encryptedValue = security.encryptData(securityKeyAlias, Json.encodeToString(value))
            editStore.invoke(it, encryptedValue.joinToString(bytesToStringSeparator))
        }
    }
}
