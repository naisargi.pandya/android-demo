package com.android_demo.helper.util

import android.view.View

object Visibility {

    fun View.visible() {
        visibility = View.VISIBLE
    }

    fun View.gone() {
        visibility = View.GONE
    }

    fun View.enable(){
        isEnabled = true
    }

    fun View.disable(){
        isEnabled = false
    }


}