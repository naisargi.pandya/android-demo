package com.android_demo.instantiation.hiltmodule

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.android_demo.BuildConfig
import com.android_demo.api.HeaderHttpInterceptor
import com.android_demo.api.service.EntryApiModule
import com.android_demo.helper.util.PrefUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RetrofitModule {
    @Singleton
    @Provides
    fun getRetrofit(prefUtil: PrefUtil) = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(
            Json.asConverterFactory("application/json".toMediaType())
        )
        .client(
            OkHttpClient.Builder()
                .addInterceptor(
                    HttpLoggingInterceptor().setLevel(
                        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                    )
                )
                .addInterceptor(HeaderHttpInterceptor(prefUtil))
                .build()
        ).build()

    @Singleton
    @Provides
    fun getEntryApiModule(retrofit: Retrofit): EntryApiModule =
        retrofit.create(EntryApiModule::class.java)
}