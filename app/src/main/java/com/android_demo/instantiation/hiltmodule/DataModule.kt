package com.android_demo.instantiation.hiltmodule

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.android_demo.AppConstants
import com.android_demo.data.room.AppDatabase
import com.android_demo.helper.util.PrefUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory
import javax.inject.Singleton
import kotlin.random.Random

@InstallIn(SingletonComponent::class)
@Module
class DataModule {
    @Singleton
    @Provides
    fun database(@ApplicationContext context: Context, prefUtil: PrefUtil): AppDatabase {
        var roomKey = ""
        if (!prefUtil.hasKey(AppConstants.Prefs.ROOM_KEY))
            roomKey = Random.nextDouble().toString()
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "room-db"
        ).openHelperFactory(
            SupportFactory(
                SQLiteDatabase.getBytes(
                    roomKey.toCharArray()
                )
            )
        ).build()
    }

    @Singleton
    @Provides
    fun dataStore(@ApplicationContext context: Context): DataStore<Preferences> =
        preferencesDataStore(name = "data-store").getValue(context, String::javaClass)
}