package com.android_demo.instantiation.hiltmodule

import com.android_demo.helper.util.SecurityUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class MiscModule {
    @Singleton
    @Provides
    fun security() = SecurityUtil()
}