package com.android_demo.main.entrymodule.model

import com.android_demo.main.base.BaseVM
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CoroutineVM
@Inject
constructor() : BaseVM() {

}