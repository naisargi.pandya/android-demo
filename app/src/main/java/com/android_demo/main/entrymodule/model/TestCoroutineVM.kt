package com.android_demo.main.entrymodule.model

import com.android_demo.main.base.BaseVM
import com.android_demo.main.entrymodule.repo.MainActRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TestCoroutineVM
@Inject
constructor(private val repo: MainActRepo) : BaseVM() {

}