package com.android_demo.main.entrymodule.state

import com.android_demo.data.model.response.LoginResModel

sealed class MainActState {
    object Idle : MainActState()
    object Loading : MainActState()
    object ValidationError : MainActState()
    data class ApiSuccess(val loginResModel: LoginResModel) : MainActState()
}