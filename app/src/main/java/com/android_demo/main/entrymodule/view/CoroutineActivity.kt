package com.android_demo.main.entrymodule.view

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import com.android_demo.R
import com.android_demo.databinding.ActivityCoroutineBinding
import com.android_demo.main.base.BaseAct
import com.android_demo.main.common.ApiRenderState
import com.android_demo.main.entrymodule.model.CoroutineVM
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

@AndroidEntryPoint
class CoroutineActivity :
    BaseAct<ActivityCoroutineBinding, CoroutineVM>(R.layout.activity_coroutine) {

    private val TAGLog: String = "CoroutineActivity"
    override val vm: CoroutineVM by viewModels()
    override val hasProgress: Boolean = true

    override fun renderState(apiRenderState: ApiRenderState) {
    }

    override fun init() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isCoRoutine()

        /*GlobalScope.launch { */
        /*    isTestCoroutine()*/
        /*}*/

        /*val sum = 10 + 20*/
        /*Thread.sleep(500)*/
        /*Log.e(TAGLog, "result sum => $sum")*/

        /*val sum2 = 20 + 20*/
        /*Thread.sleep(1000)*/
        /*Log.e(TAGLog, "result sum2 => $sum2")*/

        /*val result = sum + sum2*/
        /*Thread.sleep(4000)*/
        /*Log.e(TAGLog, "result is => $result")*/

         isApiParallel()
    }


    // we can't use suspend function outside the Coroutine's
    suspend fun isTestCoroutine(){
        delay(5000)
    }

    private fun isApiParallel() {
        GlobalScope.launch {
            Log.e(TAGLog, "Start Function => ${Thread.currentThread().name}")

            val sum = async {
                delay(1000)
                10 + 20
            }
            Log.e(TAGLog, "result global scope sum  => $sum")


            val sum1 = async {
                delay(3000)
                20 + 40
            }
            Log.e(TAGLog, "result global scope sum => $sum1")

            Log.e(TAGLog, "Wait for result ${Thread.currentThread().name}")
            val result = sum.await() + sum1.await()
            delay(5000)
            Log.e(TAGLog, "Result is  ${result}")

        }

        Log.e(TAGLog, "Hello ${Thread.currentThread().name}")
        Thread.sleep(2000)

    }

    private fun isCoRoutine() {

        runBlocking {

            Log.e(TAGLog, "Main prg start => ${Thread.currentThread().name}")

            val job: Job = launch {

                Log.e(TAGLog, "Main prg local LAUNCH start => ${Thread.currentThread().name}")

                delay(5000)

                Log.e(TAGLog, "Main prg local LAUNCH end => ${Thread.currentThread().name}")

            }

            delay(2000)
            job.join()

            GlobalScope.launch {
                Log.e(TAGLog, "Main prg  GLOBAL SCOPE start => ${Thread.currentThread().name}")
                Log.e(TAGLog, "Main prg  GLOBAL SCOPE end => ${Thread.currentThread().name}")
            }
        }

        Thread.sleep(1000)

        Log.e(TAGLog, "Main prg end => ${Thread.currentThread().name}")

    }
}