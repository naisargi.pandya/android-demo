package com.android_demo.main.entrymodule.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.activity.viewModels
import com.android_demo.Layouts
import com.android_demo.databinding.ActivityTestCoroutineBinding
import com.android_demo.main.base.BaseAct
import com.android_demo.main.common.ApiRenderState
import com.android_demo.main.entrymodule.model.TestCoroutineVM
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import java.text.DecimalFormat
import java.text.NumberFormat
import kotlin.system.measureTimeMillis

@AndroidEntryPoint
class TestCoroutineActivity :
    BaseAct<ActivityTestCoroutineBinding, TestCoroutineVM>(Layouts.activity_test_coroutine) {

    lateinit var timerFirst: CountDownTimer
    lateinit var timerSecond: CountDownTimer
    private val TAGLog: String = "TestCoroutineActivity"
    private val sum = 10 + 30

    lateinit var job: Job

    override val vm: TestCoroutineVM by viewModels()
    override val hasProgress: Boolean = true
    override fun renderState(apiRenderState: ApiRenderState) {

    }

    override fun init() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isFirstTimer()
        isSecondTimer()

        runBlocking {

            var apiResponse = measureTimeMillis {
                val one = async { isFirstApiCall() }
                val two = async { isSecondApiCall() }
                Log.e(TAGLog, "The result is ${one.await() + " and " + two.await()}")
            }
            Log.e(TAGLog, "Api Response time is => $apiResponse")

            isThirdApiCall()
            Log.e(TAGLog, " Getting Third Api call result is => $sum")

        }

    }

    suspend fun isFirstApiCall(): String {
        Log.e(TAGLog, "Getting First Result is ::")
        delay(10000)
        return "first"
    }

    suspend fun isSecondApiCall(): String {
        Log.e(TAGLog, "Getting Second Result is ::")
        delay(5000)
        return "second".plus(" Api call ")
    }

    suspend fun isThirdApiCall(): Int {
        Log.e(TAGLog, "Third Api call request ::")
        delay(5000)
        return sum
    }


    private fun isFirstTimer() {


        job = GlobalScope.launch(Dispatchers.Main) {
            try {

                timerFirst = object : CountDownTimer(20000, 1000) {
                    @SuppressLint("SetTextI18n")
                    override fun onTick(millisUntilFinished: Long) {
                        val f: NumberFormat = DecimalFormat("00")
                        val hour = millisUntilFinished / 3600000 % 24
                        val min = millisUntilFinished / 60000 % 60
                        val sec = millisUntilFinished / 1000 % 60
                        Log.e(
                            TAGLog,
                            "first counter value is => ${hour.toString() + min.toString() + sec.toString()}"
                        )
                        binding.tvMainTimer.text =
                            f.format(hour).toString() + ":" + f.format(min) + ":" + f.format(
                                sec
                            )

                    }

                    override fun onFinish() {
                        timerSecond.cancel()
                        Log.e(TAGLog, "first countdown finish ::")
                        Log.e(TAGLog, "second job finish => $job")
                    }
                }
                timerFirst.start()

            } catch (e: CancellationException) {
                Log.e(
                    TAGLog,
                    "catch first}"
                )

            } finally {
                Log.e(
                    TAGLog,
                    "finally first"
                )

            }

        }
    }


    private fun isSecondTimer() {
        job = GlobalScope.launch(Dispatchers.Main) {
            try {

                timerSecond = object : CountDownTimer(30000, 1000) {
                    @SuppressLint("SetTextI18n")
                    override fun onTick(millisUntilFinished: Long) {
                        val f: NumberFormat = DecimalFormat("00")
                        val hour = millisUntilFinished / 3600000 % 24
                        val min = millisUntilFinished / 60000 % 60
                        val sec = millisUntilFinished / 1000 % 60

                        Log.e(
                            TAGLog,
                            "second counter value is => ${hour.toString() + min.toString() + sec.toString()}"
                        )

                        binding.tvsecondTimer.text =
                            f.format(hour).toString() + ":" + f.format(min) + ":" + f.format(
                                sec
                            )
                    }

                    override fun onFinish() {
                        job.cancel()
                        binding.tvsecondTimer.text = "00:00:00"
                        Log.e(TAGLog, "Job is cancel => $job")
                        Log.e(TAGLog, "second countdown finish ::")

                    }
                }
                timerSecond.start()

            } catch (e: CancellationException) {
                Log.e(
                    TAGLog,
                    "catch second"
                )

            } finally {
                Log.e(
                    TAGLog,
                    "finally second"
                )

            }

        }

    }

}