package com.android_demo.main.entrymodule.model

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android_demo.main.base.BaseVM
import com.android_demo.main.entrymodule.view.CoroutineActivity
import dagger.hilt.android.internal.Contexts.getApplication
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class LiveDataVM
@Inject
constructor() : BaseVM() {

    private var clickCount: Int = 0
    val countLiveData = MutableLiveData<Int>()

    val editTextUserName = MutableLiveData<String>()
    val editTextUserEmail = MutableLiveData<String>()
    val editTextUserCon = MutableLiveData<String>()

    // edited user data
    private val _displayedEditTextContent = MutableLiveData<String>()
    val displayEditedData: LiveData<String>
        get() = _displayedEditTextContent

    // counter result
    private val _counterResult = MutableLiveData<String>()
    val counterResult: LiveData<String>
        get() = _counterResult


    fun isUpdateDataClick() {
        editTextUserName.value = editTextUserName.value
        editTextUserEmail.value = editTextUserEmail.value
        editTextUserCon.value = editTextUserCon.value
    }

     fun getCountPlus() {
        clickCount += 1
        _counterResult.value = clickCount.toString()
    }

     fun getCountMinus() {
         clickCount -= 1
         _counterResult.value = clickCount.toString()

    }

     fun getReset() {
        clickCount = 0
         _counterResult.value = clickCount.toString()
    }


}
