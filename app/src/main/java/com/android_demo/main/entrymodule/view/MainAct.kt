package com.android_demo.main.entrymodule.view

import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.core.text.*
import androidx.lifecycle.lifecycleScope
import com.google.firebase.messaging.FirebaseMessaging
import com.android_demo.Colors
import com.android_demo.Layouts
import com.android_demo.databinding.MainActBinding
import com.android_demo.helper.util.LocationFetchUtil
import com.android_demo.helper.util.MiscUtil.bgExecutor
import com.android_demo.helper.util.logE
import com.android_demo.main.base.BaseAct
import com.android_demo.main.common.ApiRenderState
import com.android_demo.main.entrymodule.model.MainActVM
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource

@AndroidEntryPoint
class MainAct : BaseAct<MainActBinding, MainActVM>(Layouts.act_main) {

    private lateinit var mediaPicker: EasyImage
    override val vm: MainActVM by viewModels()
    override val hasProgress: Boolean = true

    override fun init() {

        delayedExecutor(5000) {
            successToast("delayed toast")
        }

        binding.url =
            "https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"

        mediaPicker = EasyImage.Builder(this).allowMultiple(true).build()

        prefs.authToken = "s"

        //location fetch

        locationFetchUtil = LocationFetchUtil(this@MainAct,
            shouldRequestPermissions = true,
            shouldRequestOptimization = true,
            callbacks = object : LocationFetchUtil.Callbacks {
                override fun onSuccess(location: Location) {
                }

                override fun onFailed(locationFailedEnum: LocationFetchUtil.LocationFailedEnum) {
                }
            })

        binding.tv.text = buildSpannedString {
            bold { append("hi") }
            underline { append(" how are you ") }
            strikeThrough { append(" 2 ") }
            color(ContextCompat.getColor(this@MainAct, Colors.colorAccent)) { append(" nice ") }
            inSpans(object : ClickableSpan() {
                override fun onClick(textView: View) {
                    errorToast("asdasd")
                }

                override fun updateDrawState(ds: TextPaint) {
//                    ds.isUnderlineText = false
                }
            }) { bold { append(" !!!!!") } }
        }

        binding.tv.highlightColor = Color.TRANSPARENT
        /**
         * added for click to work
         */
        binding.tv.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun getFcmToken() {
        bgExecutor(this, executable = {
            FirebaseMessaging.getInstance().token.addOnSuccessListener { result ->
                if (result.isEmpty())
                //handle error
                else {
                    prefs.fcmToken = result
                    //send in api
                }
            }
        })
    }



    override fun onClick(v: View) {
        when (v.id) {
            binding.tv.id ->
                vm.login("a", "a")
//                mediaPicker.openGallery(this)
            binding.btFrag.id -> {
                prefs.userInfo =
                    com.android_demo.data.model.response.LoginResModel("abc", "abcdeee")
//                addFrag<DemoFrag>(R.id.frame)
            }
            binding.tvNext.id -> {
                startActivity(LiveDataActivity::class.java)
            }


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mediaPicker.handleActivityResult(requestCode, resultCode, data, this, object :
            EasyImage.Callbacks {
            override fun onCanceled(source: MediaSource) {

            }

            override fun onImagePickerError(error: Throwable, source: MediaSource) {

            }

            override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                imageFiles.size.toString().logE()
            }
        })
    }

    override fun renderState(apiRenderState: ApiRenderState) {
        when (apiRenderState) {
            is ApiRenderState.Loading -> showProgress()
            is ApiRenderState.ApiSuccess<*> -> {
                hideProgress()
                successToast("Success")
            }
            is ApiRenderState.ValidationError -> {
                apiRenderState.message?.let {
                    errorToast(getString(it)) { isDismissed ->
                        Toast.makeText(this, "dismissed", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }
}