package com.android_demo.main.entrymodule.model

import androidx.databinding.ObservableField
import com.android_demo.Strings
import com.android_demo.main.base.BaseVM
import com.android_demo.main.common.ApiRenderState
import com.android_demo.main.entrymodule.repo.MainActRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActVM
@Inject
constructor(private val repo: MainActRepo) : BaseVM() {

    val emailData = ObservableField("abc")

    fun login(email: String, password: String) {
        scope {
            if (email.isEmpty()) {
                state.emit(ApiRenderState.ValidationError(Strings.app_name))
                return@scope
            }
            state.emit(ApiRenderState.Loading)

            repo.login(email, password, onApiError)?.let {
                state.emit(ApiRenderState.ApiSuccess(it))
                return@scope
            }
        }
    }

    fun insertData() {
        scope {
            repo.insertData()
        }
    }

    fun nextclick() {

    }
}