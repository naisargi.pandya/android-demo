package com.android_demo.main.entrymodule.view


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android_demo.Layouts
import com.android_demo.R
import com.android_demo.databinding.ActivityLiveDataBinding
import com.android_demo.main.base.BaseAct
import com.android_demo.main.common.ApiRenderState
import com.android_demo.main.entrymodule.model.LiveDataVM
import dagger.hilt.android.AndroidEntryPoint
import kotlin.math.log

@AndroidEntryPoint
class LiveDataActivity :
    BaseAct<ActivityLiveDataBinding, LiveDataVM>(Layouts.activity_live_data) {


    override val vm: LiveDataVM by viewModels()
    override val hasProgress: Boolean = true

    override fun renderState(apiRenderState: ApiRenderState) {
    }

    override fun init() {

    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mainViewModel = ViewModelProviders.of(this)
            .get(LiveDataVM::class.java)

        DataBindingUtil.setContentView<ActivityLiveDataBinding>(
            this, R.layout.activity_live_data
        ).apply {
            this.lifecycleOwner = this@LiveDataActivity
            this.viewmodel = mainViewModel
        }

        mainViewModel.countLiveData.observe(this, Observer {
            binding.tvScore.text = it.toString()
        })

        mainViewModel.editTextUserName.observe(this, Observer {
            binding.tvUpdatedData.text = it
            Log.e("TAG","User Data Name is => ${binding.tvUpdatedData.text}")

        })

        mainViewModel.editTextUserEmail.observe(this, Observer {
            binding.tvUpdatedData.text = it
            Log.e("TAG","User Data Email is => ${binding.tvUpdatedData.text}")

        })

        mainViewModel.editTextUserCon.observe(this, Observer {
            binding.tvUpdatedData.text = it
            Log.e("TAG","User Data con is => ${binding.tvUpdatedData.text}")

        })
        binding.tvNextScreen.setOnClickListener {
            startActivity(TestCoroutineActivity::class.java)
        }

    }



}