package com.android_demo.main.entrymodule.repo

import com.android_demo.api.service.EntryApiModule
import com.android_demo.data.model.response.LoginResModel
import com.android_demo.data.room.AppDatabase
import com.android_demo.data.room.entity.DoorEntity
import com.android_demo.main.base.ApiResult
import com.android_demo.main.base.BaseRepo
import javax.inject.Inject

class MainActRepo
@Inject
constructor(private val apiCall: EntryApiModule, private val db: AppDatabase) : BaseRepo() {

    suspend fun login(
        email: String,
        password: String,
        onError: ((ApiResult<Any>) -> Unit)?
    ): LoginResModel? {
        return with(apiCall(executable = {
            apiCall.login(email, password)
        })) {
            if (data == null)
                onError?.invoke(ApiResult(null, resultType, error, resCode = resCode))
            data
        }
    }

    fun insertData() {
        db.doorDao().insertAll(DoorEntity(0, 0, "s"))
    }
}