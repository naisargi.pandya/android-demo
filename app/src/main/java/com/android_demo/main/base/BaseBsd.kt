package com.android_demo.main.base

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.android_demo.Styles

class BaseBsd() : BottomSheetDialogFragment() {

    override fun getTheme(): Int = Styles.BsdTheme

}
