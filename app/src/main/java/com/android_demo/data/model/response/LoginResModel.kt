package com.android_demo.data.model.response

import kotlinx.serialization.Serializable

@Serializable
data class LoginResModel(val name: String, val email: String)
