package com.android_demo.data.model.response

import kotlinx.serialization.Serializable

@Serializable
data class WordResModel(val name: String, val email : String, val contact : String)
