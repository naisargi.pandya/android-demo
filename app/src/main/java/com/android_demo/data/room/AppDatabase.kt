package com.android_demo.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android_demo.data.room.dao.DoorDao
import com.android_demo.data.room.entity.DoorEntity

@Database(entities = [DoorEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun doorDao(): DoorDao
}