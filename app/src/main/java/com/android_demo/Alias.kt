package com.android_demo

typealias Layouts = R.layout
typealias Drawables = R.drawable
typealias Strings = R.string
typealias Colors = R.color
typealias Dimens = R.dimen
typealias Styles = R.style