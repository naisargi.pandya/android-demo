package com.android_demo.api.service

import com.android_demo.AppConstants.Api.EndUrl.LOGIN
import com.android_demo.data.model.response.LoginResModel
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface EntryApiModule {

    @FormUrlEncoded
    @POST(LOGIN)
    suspend fun login(
        @Field("username") uName: String,
        @Field("password") pass: String
    ): LoginResModel

}



