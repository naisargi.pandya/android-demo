package com.android_demo

import androidx.datastore.preferences.core.stringPreferencesKey

class AppConstants {

    object App {
    }

    object Prefs {
        const val AUTH_TOKEN = "1"
        const val USER_INFO = "2"
        const val FCM_TOKEN = "3"
        const val ROOM_KEY = "4"
    }

    object DataStore {
        val AUTH_TOKEN = stringPreferencesKey("auth")
        val ROOM_KEY = stringPreferencesKey("room")
        val FCM_TOKEN = stringPreferencesKey("fcm")
        val USER_INFO = stringPreferencesKey("user")

    }

    object Communication {

        object RequestCode {

        }

        object ResponseCode {

        }

        object Broadcast {

        }

        object BundleData {
            const val MAIN_ACT_HEADING = "1"
            const val IS_UNAUTHORISED = "2"
        }
    }

    object Api {

        object ResponseCode {
            const val UNAUTHORIZED_CODE = 401
        }

        object EndUrl {
            const val LOGIN = "login"
            const val SIGN_UP = "signup"
        }
    }
}
