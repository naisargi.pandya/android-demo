package com.android_demo.helper.util

import org.junit.Assert.*
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

@kotlinx.serialization.Serializable
data class Test(val name: String)

class ValidationUtilTest{

    class EmailValidatorTest {

        @Test
        fun emailValidator_positive_simple() {
            val email = "savan@gmail.com"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_positive_dot() {
            val email = "savan.kashiyani@gmail.com"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_positive_dotWithSubdomain() {
            val email = "savan@yudiz.android.com"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_positive_plus_sign() {
            val email = "savan+kashiyani@gmail.com"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_positive_dash_sign() {
            val email = "savan-kashiyani@gmail.com"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_positive_digits() {
            val email = "1189@gmail.com"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_positive_underscore() {
            val email = "____@ygmail.com"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_positive_dotIn() {
            val email = "savan.k@yudiz.in"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_positive_multipleDomain() {
            val email = "savan.k@itindia.co.in"
            assertTrue(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_empty() {
            val email = ""
            //assertFalse(email.isValidEmail())
            assertEquals(false, email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_null() {
            val email: String? = null
//            assertEquals(null, email?.isEmailValid())
            assertNull(email?.isEmailValid())
        }

        @Test
        fun emailValidator_negative_without_at_sign() {
            val email = "savan.gmail.com"
            assertFalse(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_two_at_sign() {
            val email = "savan@kashiyani@gmail.com"
            assertFalse(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_without_username() {
            val email = "@gmail.com"
            assertFalse(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_only_username() {
            val email = "savankashiyani"
            assertFalse(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_invalid_characters() {
            val email = "#%^@gmail.com"
            assertFalse(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_space() {
            val email = "savan kashiyani@gmail.com"
            assertFalse(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_unicode() {
            val email = "સાવન@gmail.com"
            assertFalse(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_missing_top_domain() {
            val email = "savan@gmail"
            assertFalse(email.isEmailValid())
        }

        @Test
        fun emailValidator_negative_missing_multiple_dot_domain() {
            val email = "savan@gmail..com"
            assertFalse(email.isEmailValid())
        }
    }

    class MobileValidatorTest {

        @Test
        fun mobileValidator_positive_10_digit() {
            val mobile = "1234567890"
            assertTrue(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_alphabets() {
            val mobile = "abcdefghij"
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_alpha_numeric() {
            val mobile = "abcde12345"
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_more_digit() {
            val mobile = "12345678901"
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_less_digit() {
            val mobile = "123456789"
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_null() {
            val mobile: String? = null
            mobile?.isMobileValid()?.let { assertFalse(it) }
        }

        @Test
        fun mobileValidator_negative_empty() {
            val mobile = ""
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_symbols() {
            val mobile = "/*-++_)(*&"
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_mix_characters() {
            val mobile = "1234*67890"
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_space() {
            val mobile = "1234 67890"
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_all_space() {
            val mobile = " ".repeat(10)
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_extra_space_front() {
            val mobile = " 1234567890"
            assertFalse(mobile.isMobileValid())
        }

        @Test
        fun mobileValidator_negative_extra_space_back() {
            val mobile = "1234567890 "
            assertFalse(mobile.isMobileValid())
        }
    }

    class PasswordValidatorTest{

        @Test
        fun passwordValidator_empty(){
            val password=""
            assertFalse(password.isPasswordValid())

        }

        @Test
        fun passwordValidator_less_digit(){
            val password = "12345"
            assertFalse(password.isPasswordValid())
        }

        @Test
        fun passwordValidator_valid(){
            val password = "123456"
            assertTrue(password.isPasswordValid())
        }

    }

    class ConfirmPasswordValidatorTest{

        @Test
        fun confirmPasswordValidator_empty(){
            val confirmPassword = ""
            assertFalse(confirmPassword.isPasswordValid())
        }

        @Test
        fun confirmPassword_matches_password(){
            val confirmPassword = "12345678"
            val password="12345679"
            assertFalse(confirmPassword.matchesPassword(password))
        }

        @Test
        fun confirmPassword_valid(){
            val confirmPassword = "12345678"
            val password="12345678"
            assertTrue(confirmPassword.matchesPassword(password))
        }
    }
}

class MiscUtilTest{

    class FormatValidatorTest{

        @Test
        fun decimal_one_digit_format(){
            val number = 22.22
            val answer = number.format(1)
            assertEquals("22.2", answer)
        }

        @Test
        fun decimal_two_digit_format(){
            val number = 111.111
            val answer = number.format(2)
            assertEquals("111.11", answer)
        }

        @Test
        fun decimal_three_digit_format(){
            val number = 11.1111
            val answer = number.format(3)
            assertEquals("11.111", answer)
        }

    }

    class FirstCapitalValidatorTest{

        @Test
        fun first_small_latter(){
            val latter = "aA"
            val answer = latter.firstCapital()
            assertEquals("AA", answer)
        }

        @Test
        fun first_capital_already(){
            val latter = "Aa"
            val answer = latter.firstCapital()
            assertEquals("Aa", answer)
        }
    }


    class ConvertToModelValidatorTest{
        @Test
        fun convert_to_model(){
            val jsonString = """{"name":"Davda Parth"}"""
            val modelClass = jsonString.convertToModel<com.android_demo.helper.util.Test>()

            val actualClass = Test("Davda Parth")
            assertEquals(actualClass, modelClass)
        }
    }
}

class DateTimeUtilTest{

    class GetAgoTimeStringTest{
        @Test
        fun get_ago_time(){
            val date = Date().time
            val dateString = DateTimeUtil.getAgoTimeString(date)
            assertEquals("Just now", dateString)
        }

        @Test
        fun get_ago_time_with_wrong_input(){
            val date = Date().time
            val dateString = DateTimeUtil.getAgoTimeString(date)
            assertNotEquals("5 hours ago", dateString)
        }
    }

    class GetMillisecondsTest{
        @Test
        fun get_milli_second(){
            val dataString  = "10 Aug 2021"
            val date = dataString.getMilliseconds("dd MMM yyyy")
            assertEquals(1628533800000, date)
        }

        @Test
        fun get_mili_second_format_I(){
            val dataString = "10-08-2021"
            val date = dataString.getMilliseconds("dd-MM-yyyy")
            assertEquals(1628533800000, date)
        }
    }

    class GetDateTest {
        @Test
        fun get_date(){
            val sdf = SimpleDateFormat("dd-MM-yyyy")
            var d = Date()
            val strDate: String = sdf.format(d)
            d = sdf.parse("10-08-2021")

            val dataString = "10-08-2021"
            val date = dataString.getDate("dd-MM-yyyy")
            assertEquals(d, date)
        }
    }

    class GetDateStringTest{
        @Test
        fun get_date_string(){
            val sdf = SimpleDateFormat("dd-MM-yyyy")
            val d = Date()
            val strDate: String = sdf.format(d)

            val dateString = "dd-MM-yyyy"
            val dateResult = d.getDateString(dateString)
            assertEquals(strDate,dateResult)
        }
    }

    class ConvertDateFromStandardTimeZoneTest{
        @Test
        fun convert_date_from_standerd_time(){
            val inputFormat = "dd MMM yyyy HH:mm:ss"
            val outputFormat = "dd-MM-yyyy HH:mm:ss"
            val date = "11 Aug 2021 08:20:22"
            val dateResult = date.convertDateFromStandardTimeZone(inputFormat,outputFormat)
            assertEquals("11-08-2021 13:50:22",dateResult)
        }
    }

    class ConvertDateToStandardTimeZoneTest{
        @Test
        fun convert_date_to_standrard_time(){
            val inputFormat = "dd-MM-yyyy HH:mm:ss"
            val outputFormat = "dd MMM yyyy HH:mm:ss"
            val date = "11-08-2021 13:50:22"
            val dateResult = date.convertDateToStandardTimeZone(inputFormat,outputFormat)
            assertEquals("11 Aug 2021 08:20:22",dateResult)
        }
    }

    class ChangeDatePatternTest{
        @Test
        fun change_date_pattern(){
            val inputFormat = "dd MMM yyyy"
            val outputFormat = "dd-MM-yyyy"
            val date = "10 Aug 2021"
            val dateResult = date.changeDatePattern(inputFormat,outputFormat)
            assertEquals("10-08-2021",dateResult)
        }
    }
}