package com.android_demo.helper.util

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*

class PrefUtilTest{

    private lateinit var context: Context

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext<Context>()
    }

    @Test
    fun pref_auth_test() {
        val prefs = PrefUtil(context)
        prefs.authToken = "123456789"
        assertEquals("123456789",prefs.authToken)
    }

    @Test
    fun clear_pref_test(){
        val prefs = PrefUtil(context)
        prefs.clearPrefs()
        assertEquals("",prefs.authToken)
    }
}