# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keep class net.sqlcipher.** { *; }
-keep class net.sqlcipher.database.* { *; }

-keep class com.google.crypto.** { *; }

-keepattributes *Annotation*, InnerClasses
-dontnote kotlinx.serialization.AnnotationsKt

-keepclassmembers class kotlinx.serialization.json.** {
    *** Companion;
}
-keepclasseswithmembers class kotlinx.serialization.json.** {
    kotlinx.serialization.KSerializer serializer(...);
}

-keep,includedescriptorclasses class com.android_demo.**$$serializer { *; }
-keepclassmembers class com.android_demo.** {
    *** Companion;
}
-keepclasseswithmembers class com.android_demo.** {
    kotlinx.serialization.KSerializer serializer(...);
}

# GSON
-keepnames class com.google.gson.* {}
-keepnames enum com.google.gson.* {}
-keepnames interface com.google.gson.* {}
-keep class com.google.gson.* {}
-keepnames class com.android_demo.data.model.* {}
-keepnames enum com.android_demo.data.model.* {}
-keepnames interface com.android_demo.data.model.* {}
-keep class com.android_demo.data.model.* {}
