import java.text.SimpleDateFormat
import java.util.*

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
    kotlin("plugin.serialization") version "1.4.10"
//    id("com.google.firebase.crashlytics")
}

android {

    defaultConfig {
        val appName = "Android Demo"
        val vCode = 1
        val vName = "1.0.0"

        applicationId = "com.android_demo.App"

        minSdk = 23
        compileSdk = 32
        targetSdk = 32

        versionCode = vCode
        versionName = vName

        multiDexEnabled = true
        buildFeatures.dataBinding = true

        resValue("string", "app_name", appName)
        manifestPlaceholders["appName"] = appName
        base.archivesBaseName =
            "${appName}_${vName}(v${vCode})_${SimpleDateFormat("dd-MMM-yyyy").format(Date())}"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName("debug") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    flavorDimensions("variant")
    productFlavors {
        create("staging") {
            applicationIdSuffix = ".staging"
            versionNameSuffix = "-staging"
            buildConfigField("String", "BASE_URL", "\"http://abc.xyz/\"")
        }

        create("production") {
            versionNameSuffix = "-production"
            buildConfigField("String", "BASE_URL", "\"http://abcd.xyz/\"")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().all {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}

dependencies {
    //kotlin
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.5.21")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.5.1")

    //android
    implementation("androidx.appcompat:appcompat:1.3.1")
    implementation("com.google.android.material:material:1.4.0-alpha02")
    implementation("androidx.multidex:multidex:2.0.1")
    implementation("androidx.constraintlayout:constraintlayout:2.1.0")
    implementation("androidx.viewpager2:viewpager2:1.0.0")

    //fragments
    implementation("androidx.fragment:fragment-ktx:1.3.6")

    //di
//    implementation("org.koin:koin-androidx-viewmodel:2.1.5")
    implementation("com.google.dagger:hilt-android:${Constants.hiltVersion}")
    kapt("com.google.dagger:hilt-android-compiler:${Constants.hiltVersion}")

    //architecture components
    val archCompVersion = "2.3.0-beta01"
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$archCompVersion")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$archCompVersion")

    //dimension
    /*implementation 'com.intuit.sdp:sdp-android:1.0.6'
    implementation 'com.intuit.ssp:ssp-android:1.0.6'*/

    //recyclerview decoration
    implementation("com.github.fondesa:recycler-view-divider:3.3.0")

    //image loading
    implementation("io.coil-kt:coil:0.11.0")

    //api
    val retrofitVersion = "2.8.1"
    implementation("com.squareup.okhttp3:logging-interceptor:4.9.0")
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:0.8.0")

    //serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.2")

    //play services
    implementation("com.google.android.gms:play-services-maps:17.0.1")
    implementation("com.google.android.gms:play-services-location:18.0.0")

    //firebase
    implementation(platform("com.google.firebase:firebase-bom:26.0.0"))
    implementation("com.google.firebase:firebase-messaging")
    implementation("com.google.firebase:firebase-analytics-ktx")
    implementation("com.google.firebase:firebase-crashlytics")
//    implementation("com.google.firebase:firebase-crashlytics:17.1.1")

    //data
    val roomVersion = "2.4.2"
    implementation("androidx.room:room-runtime:$roomVersion")
    kapt("androidx.room:room-compiler:$roomVersion")
    implementation("androidx.datastore:datastore-preferences:1.0.0")

    //compressor
    implementation("id.zelory:compressor:3.0.1")

    //memory leak detection
//    debugImplementation("com.squareup.leakcanary:leakcanary-android:2.0-beta-3")

    //image picker
    implementation("com.github.jkwiecien:EasyImage:3.1.0")

    //security
    implementation("androidx.security:security-crypto:1.1.0-alpha03")
    implementation("net.zetetic:android-database-sqlcipher:4.3.0")

    //permission
    implementation("com.github.smokelaboratory:freedom:1.0.0")


    //test
    // Local Unit Tests
//    implementation "androidx.test:core:1.4.0"
    testImplementation("junit:junit:4.13.2")
//    testImplementation "org.hamcrest:hamcrest-all:1.3"
//    testImplementation "androidx.arch.core:core-testing:2.1.0"
//    testImplementation "org.robolectric:robolectric:4.4"
//    testImplementation "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.5.0"
    testImplementation("com.google.truth:truth:1.0.1")
    testImplementation("org.mockito:mockito-core:3.5.13")

    // Instrumented Unit Tests
    androidTestImplementation("junit:junit:4.13.2")
//    androidTestImplementation "com.linkedin.dexmaker:dexmaker-mockito:2.12.1"
//    androidTestImplementation "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.5.0"
//    androidTestImplementation "androidx.arch.core:core-testing:2.1.0"
    androidTestImplementation("com.google.truth:truth:1.0.1")
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
//    androidTestImplementation "androidx.test:runner:1.4.0"
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
    androidTestImplementation("org.mockito:mockito-core:3.5.13")
    //test
}
